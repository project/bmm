<?php

/**
 * @file
 * Contains bmm.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 */
function bmm_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the bmm module.
    case 'help.page.bmm':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides workflow for currently logged in users during deployment process.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function bmm_form_system_site_maintenance_mode_alter(&$form, FormStateInterface $form_state, $form_id) {

  $form['logout'] = [
    '#type' => 'details',
    '#title' => t('Extra settings'),
  ];

  $config = \Drupal::config('bmm.settings');
  $complete_base_url = \Drupal::service('router.request_context')->getCompleteBaseUrl();

  $form['logout']['description'] = [
    '#type' => 'markup',
    '#markup' => t("You can define a message to users who are already logged in and save the form without putting your site into a maintanance mode and without checking `Log out all user who are currently logged in.` checkbox. This way you inform your users about upcoming maintanence mode. At certain point of time just put a site into a maintanace mode and check `Log out all user who are currently logged in.`. This way all users (except admin) are going to be logged out and your site will switch into a maintanace mode. Additionally `/user` (user.login) route is not going to be available, so users are not allowed to log in. In order to log in again you need to disable maintanance mode with drush or you can use a hidden page for login - to setup a new hidden path provide a hash into `Hash for hidden login page` value or leave the default one. Once it's there you can access hidden login page during maintanace mode enabled by visiting @site/hidden/login/{hash}", ['@site' => $complete_base_url]),
  ];

  $form['logout']['logout_message'] = [
    '#type' => 'textarea',
    '#title' => t('Upcoming maintenance mode message'),
    '#description' => t('You can leave a message to logged in users to save their work before you put a site into a maintanace mode and clean their sessions. Example: Please save your work and log out of the system, as at 10:00 EST we are going to swich to maintanace mode. Right after switching to maintanace mode logged in users are going to be logged out automatically.'),
    '#default_value' => $config->get('logout_message'),
  ];

  $form['logout']['logout_users'] = [
    '#type' => 'checkbox',
    '#title' => t('Log out all user who are currently logged in.'),
    '#description' => t("This option doesn't save anything. It clears currently storred sessions immediately on form submit. You can also use drush to logout users with `bmm:sessions:clear`."),
  ];

  $form['logout']['login_hash'] = [
    '#type' => 'textfield',
    '#title' => t('Hash for hidden login page'),
    '#required' => TRUE,
    '#description' => t('Provide a hash to make hidden login page available during maintanance mode by visiting @site/hidden/login/{hash}', ['@site' => $complete_base_url]),
    '#default_value' => $config->get('hash'),
  ];

  $form['#submit'][] = 'bmm_logout_users';
}

/**
 * Submit function().
 */
function bmm_logout_users($form, FormStateInterface $form_state) {
  $values = $form_state->getValues();

  $config = \Drupal::service('config.factory')->getEditable('bmm.settings');
  $config->set('logout_message', $values['logout_message'])->save();
  $config->set('hash', $values['login_hash'])->save();

  // If there's a logout checkbox checked.
  if (!empty($values['logout_users'])) {
    // Get current sessions except the one for admin.
    $query = \Drupal::database()->select('sessions', 's');
    $query->leftJoin('users_field_data', 'u', 'u.uid = s.uid');
    $query->condition('s.uid', 1, '<>');
    $query->fields('s', ['uid']);
    $query->fields('u', ['name']);
    $result = $query->execute()->fetchAll();
    // Destroy sessions.
    if (!empty($result)) {
      foreach ($result as $session) {
        $session_manager = \Drupal::service('session_manager');
        $session_manager->delete($session->uid);
      }
      \Drupal::messenger()->addMessage(t('@number of users has been logged out.', ['@number' => count($result)]));
    }
  }
}

/**
 * Implements hook_preprocess_page().
 */
function bmm_preprocess_page(&$variables) {
  $config = \Drupal::config('bmm.settings');
  if (!empty($config->get('logout_message')) && \Drupal::currentUser()->isAuthenticated()) {
    \Drupal::messenger()->addWarning($config->get('logout_message'));
  }
}
