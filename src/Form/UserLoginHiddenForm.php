<?php

namespace Drupal\bmm\Form;

use Drupal\Core\Url;
use Drupal\user\Form\UserLoginForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\user\UserStorageInterface;
use Drupal\user\UserAuthInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a user login hidden form.
 */
class UserLoginHiddenForm extends UserLoginForm {
  /**
   * The state keyvalue collection.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a UserLoginHiddenForm.
   */
  public function __construct(FloodInterface $flood, UserStorageInterface $user_storage, UserAuthInterface $user_auth, RendererInterface $renderer, StateInterface $state) {
    parent::__construct($flood, $user_storage, $user_auth, $renderer);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('flood'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('user.auth'),
      $container->get('renderer'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $hash = '') {
    $form = parent::buildForm($form, $form_state);
    $config = \Drupal::config('bmm.settings');
    if (!empty($config->get('hash')) && $hash == $config->get('hash') && $this->state->get('system.maintenance_mode') == '1') {
      return $form;
    }
    else {
      return new RedirectResponse(Url::fromRoute('<front>', [], ['absolute' => FALSE])->toString());
    }
  }

}
