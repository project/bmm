<?php

namespace Drupal\bmm\Drush\Commands;

use Drush\Commands\DrushCommands;
use Drush\Attributes as CLI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SessionsCommand.
 */
final class SessionsCommand extends DrushCommands {

  /**
   * The database connection to use.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a SessionsCommand object.
   */
  public function __construct($connection, $entity_type_manager) {
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Drush command clear current users sessions.
   */
  #[CLI\Command(name: 'bmm:sessions:clear', aliases: ['bmmsc'])]
  #[CLI\Usage(name: 'bmm:sessions:clear', description: 'Clear current users sessions')]
  public function clear() {
    $query = $this->connection->select('sessions', 's');
    $query->leftJoin('users_field_data', 'u', 'u.uid = s.uid');
    $query->condition('s.uid', 1, '<>');
    $query->fields('s', ['uid']);
    $query->fields('u', ['name']);
    $result = $query->execute()->fetchAll();
    // Destroy sessions.
    if (!empty($result)) {
      foreach ($result as $session) {
        // We can't use session_manager here because it has $this->isCli()
        // condition which does not allow to clear the session. That is why
        // it's set to direct query as Drupal\Core\Session delete method does.
        // Check https://bit.ly/37fsvSG for details.
        $this->connection
          ->delete('sessions')
          ->condition('uid', $session->uid)
          ->execute();
      }
      $this->logger()->success(dt('@number of users has been logged out.', ['@number' => count($result)]));
    }
    else {
      $this->logger()->success(dt('None of the users has been logged in.'));
    }
  }

}
